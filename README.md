# Sound Effect Variations

Easily add variations to your sound effects. Plays a random `AudioClip` from a
list, and randomly adjusts their pitch so the sound effects don't get
repetitive.

# Installation

## For Unity 2019.3 or Later

[Official instructions](https://docs.unity3d.com/Manual/upm-ui-giturl.html)

1. Open the Package Manager by selecting `Window` -> `Package Manager` from the
   Unity menu.
2. Click the `+` button on the top-left corner of the Package Manager tab then
   select `Add package from git URL`.
3. Enter `https://gitlab.com/TwoScoopGames/sound-effect-variations.git`, and
   click `Add`

## For Unity pre-2019.3

Add this package's git URL to your `Packages/manifest.json` file:

```json
{
  "dependencies": {
    "com.twoscoopgames.soundeffectvariations": "https://gitlab.com/TwoScoopGames/sound-effect-variations.git"
  }
}
```

# Usage

1. Create a `SoundEffectVariations` `ScriptableObject` by right-clicking in the
  project tab, then selecting `Create` -> `ScriptableObject` ->
  `SoundEffectVariations`.

![Screenshot of menu for creating
SoundEffectVariations](/Images/create-sound-effect-variation.png)

2. Add [AudioClip](https://docs.unity3d.com/ScriptReference/AudioClip.html)s by
  selecting the new `SoundEffectVariations` and dragging `AudioClip`s to the Clips
  property in the inspector.

![Screenshot showing inspector view of
SoundEffectVariations](/Images/add-audio-clips.png)

Now you can set the [Play Mode](#play-mode), and random pitch settings for the variations.

3. Create a `SoundEffectVariations` property in a script, and call
  `SoundEffectVariations.Play(AudioSource)` to play a sound effect.

![Screenshot showing a property drawer for a
SoundEffectVariations](/Images/property-drawer.png)

A sample script might look like:

```csharp
using TwoScoopGames.SoundEffectVariations;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundEffect : MonoBehaviour {
  private AudioSource audioSource;
  public SoundEffectVariations jumpSoundEffect;

  void Start() {
    audioSource = GetComponent<AudioSource>();
  }

  public void Play() {
    jumpSoundEffect.Play(audioSource);
  }
}
```

# SoundEffectVariations

## Properties

* `AudioClip[] clips`: A clip will randomly be picked from this list.
* `PlayMode playMode`: How to handle multiple sound effects being played at the
  same time. See [Play Mode](#play-mode).
* `float maximumPitch`: The maximum pitch to randomly pick. See [AudioSource.pitch](https://docs.unity3d.com/ScriptReference/AudioSource-pitch.html).
* `float minimumPitch`: The minimum pitch to randomly pick. See [AudioSource.pitch](https://docs.unity3d.com/ScriptReference/AudioSource-pitch.html).

## Methods

* `void Play(AudioSource audioSource)`: play a random `AudioClip` at a random
  pitch on the given `audioSource`. You can share one `AudioSource` amongst
  multiple `SoundEffectVariations` that have `playMode` set to `PlayMode.Overlap`,
  but `SoundEffectVariations` with other play modes should use a dedicated
  `AudioSource`s.
* `void Stop(AudioSource audioSource)`: stop playing the current `AudioClip` on
  the `AudioSource`.

# Play Mode

* `PlayMode.Overlap`: Multiple sound effects may be played at the same time.
* `PlayMode.WaitUntilFinished`: Sound effects will not play while another sound
  effect is already playing.
* `PlayMode.Restart`: Playing a sound effect will cancel the currently playing
  sound effect.
