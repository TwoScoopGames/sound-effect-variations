﻿using UnityEditor;
using UnityEngine;

namespace TwoScoopGames.SoundEffectVariations.Editor {
  [CustomEditor(typeof(SoundEffectVariations))]
  public class SoundEffectVariationsEditor : UnityEditor.Editor {

    private GameObject audioSourceContainer;
    private AudioSource audioSource;

    public override void OnInspectorGUI() {
      Texture2D icon = EditorGUIUtility.FindTexture("PlayButton");

      if (GUILayout.Button(new GUIContent() { image = icon, text = "Play" })) {
        var e = target as SoundEffectVariations;
        e.Play(audioSource);
      }

      base.OnInspectorGUI();
    }

    void OnEnable() {
      var e = target as SoundEffectVariations;
      audioSourceContainer = new GameObject(string.Format("AudioSource Container ({0})", target.name));
      audioSourceContainer.hideFlags = HideFlags.HideAndDontSave;
      audioSource = audioSourceContainer.AddComponent<AudioSource>();
    }

    void OnDisable() {
      GameObject.DestroyImmediate(audioSourceContainer);
      audioSourceContainer = null;
      audioSource = null;
    }
  }
}
