﻿using UnityEditor;
using UnityEngine;

namespace TwoScoopGames.SoundEffectVariations.Editor {
  [CustomPropertyDrawer(typeof(SoundEffectVariations))]
  public class SoundEffectVariationsDrawer : PropertyDrawer {

    private GameObject audioSourceContainer;
    private AudioSource audioSource;

    Texture2D playIcon = EditorGUIUtility.FindTexture("PlayButton");

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
      var buttonWidth = 32;
      var padding = 2;

      var propertyRect = new Rect(position.x, position.y, position.width - buttonWidth - padding, 16);
      EditorGUI.PropertyField(propertyRect, property);

      var buttonRect = new Rect(position.x + position.width - buttonWidth, position.y, buttonWidth, 16);
      if (GUI.Button(buttonRect, playIcon)) {
        Play(property);
      }
    }

    private void Play(SerializedProperty property) {
      if (audioSourceContainer == null) {
        audioSourceContainer = new GameObject(string.Format("AudioSource Container ({0})", property.name));
        audioSourceContainer.hideFlags = HideFlags.HideAndDontSave;
        audioSource = audioSourceContainer.AddComponent<AudioSource>();
      }

      var variations = property.objectReferenceValue as SoundEffectVariations;
      if (variations) {
        variations.Play(audioSource);
      }
    }

    void OnDisable() {
      GameObject.DestroyImmediate(audioSourceContainer);
      audioSourceContainer = null;
      audioSource = null;
    }
  }
}
