using Random = UnityEngine.Random;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace TwoScoopGames.SoundEffectVariations {
  [CreateAssetMenu(menuName="ScriptableObjects/SoundEffectVariations")]
  public class SoundEffectVariations : ScriptableObject {

    public enum PlayMode {
      Overlap,
      WaitUntilFinished,
      Restart,
    }

    public AudioClip[] clips;
    public PlayMode playMode = PlayMode.Overlap;
    public float maximumPitch = 1f;
    public float minimumPitch = 1f;

    #if UNITY_EDITOR
    [NonSerialized]
    private static Dictionary<AudioSource, SoundEffectVariations> sharedAudioSources = new Dictionary<AudioSource, SoundEffectVariations>();
    #endif

    public void Play(AudioSource audioSource) {
      if (clips.Length == 0) {
        return;
      }

      #if UNITY_EDITOR
      if (sharedAudioSources.ContainsKey(audioSource) &&
          playMode != PlayMode.Overlap &&
          sharedAudioSources[audioSource] != this) {
          Debug.LogError(string.Format("SoundEffectVariations({0}) in {1} mode shared an AudioSource with {2} which will cause audio problems", name, playMode, sharedAudioSources[audioSource].name));
      } else {
        sharedAudioSources[audioSource] = this;
      }
      #endif

      if (playMode == PlayMode.WaitUntilFinished && audioSource.isPlaying) {
        return;
      }

      int index = Random.Range(0, clips.Length);
      var clip = clips[index];

      audioSource.pitch = Random.Range(minimumPitch, maximumPitch);

      switch (playMode) {
        case PlayMode.Overlap:
          audioSource.PlayOneShot(clip);
          break;
        default:
          audioSource.clip = clip;
          audioSource.Play();
          break;
      }
    }

    public void Stop(AudioSource audioSource) {
      audioSource.Stop();
    }
  }
}
